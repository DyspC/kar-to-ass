export function convertKarToAss(fileContent: KarData, options: Options): string;

export function parseKar(fileContent: Buffer): KarData;

export function convertKarStringToAss(fileContent: Buffer, options: Options);

export interface KarData {
  timebase: KarTimebase;
  tracks: KarTrack[];
}

export interface KarTimebaseSection {
  tickStart: number;
  tickEnd: number;
  microSecondsPerTick: number;
}

export type KarTimebase = KarTimebaseSection[];
export type KarTrack = KarEvent[];

export interface KarEvent {
  ticks: number;
  timeUs?: number; // µs
  text?: string;
  type: KarEventType;
}
export type KarEventType = 'EndOfLine' | 'Lyrics';

export interface Options {
  offset?: number; // default 0
  progressive?: boolean; // default false
}
