# kar-to-ass

Conversion tool made to parse kar files and rewrite their lyrics using ASS format

## Usage

### CLI

```bash
# Prints conversion on stdout
kar-to-ass -i <kar file>

# Prints conversion into specified file
kar-to-ass -i <kar file> -o <ASS file>

# Use 'kf' instead of 'k' to make the text filling progressive
kar-to-ass -i <kar file> -p

# Converts file and applies the specified offset (in cs)
kar-to-ass -i <kar file> -c <offset>

# Displays more help
kar-to-ass --help
```

### Module

#### function parseKar(fileContent: string): KarData

Loads a `KarData` structure based on said `fileContent`

#### function convertKarToAss(kar: KarData, options: Options): string

Prepares said `kar` and returns it as a `string`

#### function convertKarStringToAss(fileContent: string, options: Options): string

All of the above in one step

#### interface Options

Wraps all options that could be used during conversion. Its members are:
- offset (number): offsets every line
- progressive (boolean): use 'kf' instead of 'k'

## Test

You can test code with a kar file :

```sh
node dist/index.js -i test.kar -o test.ass
```